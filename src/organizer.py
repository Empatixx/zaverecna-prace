"""
File Organizer Script

This script organizes files in a specified directory into subdirectories based on file types.
It uses logging to keep track of the files being moved.

Usage:
    python organizer.py <directory>
"""

import os
import shutil
import logging
import argparse


def setup_logger():
    """
    Set up the logger to log information to 'logs/organizer.log'.
    """
    logging.basicConfig(filename='logs/organizer.log', level=logging.INFO,
                        format='%(asctime)s:%(levelname)s:%(message)s')


def move_files(directory):
    """
    Move files into folders based on their file type.

    Args:
        directory (str): The directory to organize.
    """
    file_types = {
        '.jpg': 'Images',
        '.txt': 'Texts',
        '.pdf': 'Documents',
        # Add more file types and folders as needed
    }

    for root, _, files in os.walk(directory):
        for file in files:
            file_ext = os.path.splitext(file)[1]
            if file_ext in file_types:
                dest_dir = os.path.join(directory, file_types[file_ext])
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)
                shutil.move(os.path.join(root, file), dest_dir)
                logging.info('Moved %s to %s', file, dest_dir)


def parse_arguments():
    """
    Parse command-line arguments.

    Returns:
        argparse.Namespace: The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description='Automatically organize files into folders based on file type.')
    parser.add_argument('directory', type=str, help='Directory to organize')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    setup_logger()
    move_files(args.directory)
