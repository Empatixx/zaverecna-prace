#!/bin/bash

mkdir -p logs

# Check if virtualenv is installed, if not, install it
if ! command -v virtualenv &> /dev/null; then
    echo "virtualenv not found, installing..."
    pip install virtualenv
else
    echo "virtualenv is already installed."
fi

# Check if the virtual environment directory exists
if [ ! -d "venv" ]; then
    echo "Creating virtual environment..."
    virtualenv venv
else
    echo "Virtual environment already exists."
fi

echo "Activating virtual environment..."
source venv/bin/activate

echo "Installing dependencies from requirements.txt..."
pip install -r requirements.txt

echo "Setup complete."
