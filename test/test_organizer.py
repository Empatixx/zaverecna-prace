"""
Unit tests for the File Organizer Script

This script tests the functionality of the file organizer script to ensure that files
are correctly moved into their respective folders based on file types.
"""

import unittest
import os
import shutil
from src.organizer import move_files  # Adjust the import path as necessary


class TestFileOrganizer(unittest.TestCase):
    """
    Test case for the file organizer functionality.
    """
    test_dir = "test_directory"

    @classmethod
    def setUpClass(cls):
        """
        Set up a directory structure for testing file movements.
        """
        os.makedirs(cls.test_dir, exist_ok=True)
        # Create test files of different types
        with open(os.path.join(cls.test_dir, "test1.jpg"), "w", encoding='utf-8') as f:
            f.write("This is a test image file.")
        with open(os.path.join(cls.test_dir, "test2.txt"), "w", encoding='utf-8') as f:
            f.write("This is a test text file.")
        with open(os.path.join(cls.test_dir, "test3.pdf"), "w", encoding='utf-8') as f:
            f.write("This is a test document.")

    @classmethod
    def tearDownClass(cls):
        """
        Clean up: Remove the directory structure created for testing.
        """
        shutil.rmtree(cls.test_dir)

    def test_file_movement(self):
        """
        Test if files are moved to the correct directories based on their file type.
        """
        move_files(self.test_dir)
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, "Images", "test1.jpg")))
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, "Texts", "test2.txt")))
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, "Documents", "test3.pdf")))


if __name__ == '__main__':
    unittest.main()
