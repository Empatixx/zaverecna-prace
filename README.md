# File Organizer Script

This script organizes files in a specified directory into subdirectories based on file types. It uses logging to keep
track of the files being moved.

## Prerequisites

- Python 3.x
- `pip`

## Setup

1. **Clone the repository and navigate to the project directory.**

2. **Run the setup script** to create a virtual environment, install necessary dependencies, and prepare the logging
   directory.

```bash
./scripts/setup.sh
```

## Usage

After setting up the environment, you can organize your files by running the following command:

```bash
bash ./scripts/run.sh <directory>
```

Replace `<directory>` with the path to the directory you want to organize.
